include <configuration.scad>
include <general.scad>
include <backplate.scad>
include <verticalplate.scad>
include <horisontalplate.scad>
include <horisontalseparator.scad>
include <verticalseparator.scad>


module PlaceVerticalSeparator() {
    color([1, 0.8, 0.8, 0.6]) {
        for(i = [1:len(drawerWidths)-1]) {
            x = - separatorThickness + // Get it to the correct side of the axis
                    sidePlateThickness + // Get it past the side plate
                    sum(drawerWidths, i) + // Get it past all columns above
                    i*(2*drawerToRoomMargin+separatorThickness);  // ... and their margins

            translate([x, 0, 0]) rotate([90, 0, 90]) 
                MakeVerticalSeparator();
        }
    }
}

module MakeVerticalSeparator() {
    xSize = height();
    ySize = backPlateThickness+drawerDepth; // depth

    difference() {
        verticalSeparator(xSize, ySize, separatorThickness);
        verticalSeparatorFingers(xSize, ySize, drawerHeights, drawerRoomDepth, separatorThickness, backPlateThickness, sidePlateThickness, drawerToRoomMargin);
        verticalSeparatorSlots(drawerHeights, separatorThickness, drawerDepth, sidePlateThickness, backPlateThickness, drawerToRoomMargin);
    }
}

module Make2DVerticalSeparator() {
    projection() MakeVerticalSeparator();
}

module PlaceHorisontalSeparator() {
    color([0.8, 0.8, 1, 0.6]) {
        for(i = [1:len(drawerHeights)-1]) {
            y = sidePlateThickness + // Get it past the side plate
                    sum(drawerHeights, i) + // Get it past all rows above
                    i*(2*drawerToRoomMargin+separatorThickness);  // ... and their margins

            translate([0, y, 0]) 
                rotate([90, 0, 0]) 
                    MakeHorisontalSeparator();
        }   
    }
}

module MakeHorisontalSeparator() {
    xSize = width();
    ySize = backPlateThickness+drawerDepth; // depth

    difference() {
        horisontalSeparator(xSize, ySize, separatorThickness);
        horisontalSeparatorFingers(xSize, ySize, drawerWidths, drawerRoomDepth, separatorThickness, backPlateThickness, sidePlateThickness, drawerToRoomMargin);
        horisontalSeparatorSlots(drawerWidths, separatorThickness, drawerDepth, sidePlateThickness, backPlateThickness, drawerToRoomMargin);
    }
}

module Make2DHorisontalSeparator() {
    projection() MakeHorisontalSeparator();
}

module PlaceHorisontalPlate() {
    color([0.3, 0, 1]) {
        translate([0, sidePlateThickness, 0]) rotate([90, 0, 0]) 
            MakeHorisontalPlate();
        translate([0, height(), 0]) rotate([90, 0, 0]) 
            MakeHorisontalPlate();
    }
}

module MakeHorisontalPlate() {
    xSize = width();
    ySize = drawerRoomDepth+backPlateThickness; // depth

    difference() {
        horisontalPlate(xSize, ySize, sidePlateThickness);
        horisontalPlateFingers(xSize, ySize, sidePlateThickness, rackFingerLength, sidePlateThickness, backPlateThickness);
        horisontalPlateMortises(drawerWidths, drawerRoomDepth, sidePlateThickness, backPlateThickness, separatorThickness, drawerToRoomMargin);
    }
    // TODO: If the depth is an odd multiple of the finger width, the last dogbone should not be cut out.
}

module Make2DHorisontalPlate() {
    projection() MakeHorisontalPlate();
}

module PlaceVerticalPlate() {
    color([1, 0, 0]) {
        translate([sidePlateThickness, 0, 0]) rotate([0, -90, 0]) 
            MakeVerticalPlate();
        translate([width(), 0, 0]) rotate([0, -90, 0]) 
            MakeVerticalPlate();
    }
}

module MakeVerticalPlate() {
    ySize = height();
    xSize = drawerRoomDepth+backPlateThickness; // depth

    difference() {
        verticalPlate(xSize, ySize, sidePlateThickness);
        verticalPlateFingers(xSize, ySize, sidePlateThickness, rackFingerLength, sidePlateThickness, backPlateThickness);
        verticalPlateMortises(drawerRoomDepth, drawerHeights, sidePlateThickness, backPlateThickness, separatorThickness, drawerToRoomMargin);
    }
    // TODO: If the depth is an odd multiple of the finger width, the last dogbone should not be cut out.
}

module Make2DVerticalPlate() {
    projection() MakeVerticalPlate();
}

module PlaceBackPlate() {
    translate([0, 0, 0])
        MakeBackPlate();
}

module MakeBackPlate() {
    xSize = width();
    ySize = height();

    echo("Backplate: ", xSize, ySize);

    difference() {
        backPlate(xSize, ySize, backPlateThickness);
        backPlateFingers(xSize, ySize, backPlateThickness, rackFingerLength, sidePlateThickness);
        backPlateMortises(drawerWidths, drawerHeights, backPlateThickness, sidePlateThickness, separatorThickness, drawerToRoomMargin);
    }
}

module Make2DBackPlate() {
    projection() MakeBackPlate();
}
