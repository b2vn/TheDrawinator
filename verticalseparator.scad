include <fingers.scad>
include <horisontalseparator.scad>

module verticalSeparator(xSize, ySize, thickness) {
    horisontalSeparator(xSize, ySize, thickness);
}

module verticalSeparatorFingers(xSize, ySize, widths, depth, thickness, fingerDepthX, fingerDepthY, margin) {
    // The finger are the same on the horisontal and the vertical separators. The only difference is the way the plates are rotated in the "Make" methods. This proncipple should also be used for the sides.
    horisontalSeparatorFingers(xSize, ySize, widths, depth, thickness, fingerDepthX, fingerDepthY, margin);
}

module verticalSeparatorSlots(widths, thickness, depth, offsetX, offsetY, margin) {
    // The vertical separaters is cut from the back, to ensure maximum strength on the front side
    for(i = [1:len(widths)-1]) {
        x = offsetX
                + sum(widths, i) 
                + i*(thickness+2*margin);
        translate([x-thickness, offsetY, 0]) 
            rotate([0, 0, 0])
                finger(thickness, depth/2, thickness);
    }
}