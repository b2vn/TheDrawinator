include <fingers.scad>

module horisontalPlate(xSize, ySize, thickness) {
    xSize = xSize+2*fingerExtraLength;
    ySize = ySize+fingerExtraLength;
    translate([-fingerExtraLength, -fingerExtraLength, 0])
        cube([xSize, ySize, thickness]);
}

module horisontalPlateFingers(xSize, ySize, thickness, fingerWidth, fingerDepth1, fingerDepth2) {
    fx = fingerSizeOdd(xSize, fingerWidth);
    fy = fingerSize(ySize, fingerWidth);
    echo("Horisontal/front plate finger size (depth/height, width):", fy, fx);

    fingerDepth1=fingerDepth1+fingerExtraLength;
    fingerDepth2=fingerDepth2+fingerExtraLength;

    translate([fingerExtraLength, 0, 0])
        plateFingers(xSize, ySize, thickness, fx, fy, fingerDepth1, fx, fy, false, true, false, false);
    translate([-fingerExtraLength, 0, 0])
        plateFingers(xSize, ySize, thickness, fx, fy, fingerDepth1, fx, fy, true, false, false, false);
    translate([0, -fingerExtraLength, 0])
        plateFingers(xSize, ySize, thickness, fx, fy, fingerDepth2, fx, fy, false, false, false, true);
}

module horisontalPlateMortises(widths, heights, depth, offset, thickness, margin) {
    // Mortises for vertical separaters
    for(i = [1:len(widths)-1]) {
        x = depth
                + sum(widths, i) 
                + i*(thickness+2*margin)
                - thickness;
        translate([x, offset+heights/4, 0]) 
            finger(thickness, heights/2, depth, db["xy"], false);
        // TODO: Should the separators have fingers on the sides instead?
        }
}