module plateFingers(xSize, ySize, thickness, fingerWidthX, fingerWidthY, fingerDepth, 
        xOffset=0, yOffset=0, left=true, right=true, top=true, bottom=true) {
    for(x = [xOffset-MAGIC_OFFSET_4 : 2*fingerWidthX : xSize-MAGIC_OFFSET]) {
        if(bottom)
            translate([x, 0, 0]) 
                finger(fingerWidthX, fingerDepth, thickness, db["xy"]);
        if(top)
            translate([x+fingerWidthX, ySize+MAGIC_OFFSET_2, 0]) 
                rotate([0, 0, 180])
                    finger(fingerWidthX, fingerDepth, thickness, db["xy"]);
    }
    for(y = [yOffset+MAGIC_OFFSET_2 : 2*fingerWidthY : ySize-MAGIC_OFFSET]) {
        if(left)
            translate([-MAGIC_OFFSET_2, y+fingerWidthY, 0]) 
                rotate([0, 0, -90])
                    finger(fingerWidthY, fingerDepth, thickness, db["xy"]);
        if(right)
            translate([xSize+MAGIC_OFFSET_2, y, 0]) 
                rotate([0, 0, 90])
                    finger(fingerWidthY, fingerDepth, thickness, db["xy"]);
    }
}


db= [ ["x",0], ["y",1], ["xy",2] ];
module finger(xSize, ySize, thickness, d1=db["xy"], onlyOneSide=true) {
    dbDepth = 0.707*toolRadius;
    translate([-MAGIC_OFFSET_2-fingerPlayRoom/2, -MAGIC_OFFSET_2, -MAGIC_OFFSET_2]) {
        cube([xSize+MAGIC_OFFSET+fingerPlayRoom, ySize+MAGIC_OFFSET, thickness+MAGIC_OFFSET]);

        translate([xSize-dbDepth, ySize-dbDepth, 0]) 
            cylinder(r=toolRadius, h=thickness+MAGIC_OFFSET);
        translate([dbDepth, ySize-dbDepth, 0]) 
            cylinder(r=toolRadius, h=thickness+MAGIC_OFFSET);

        if(!onlyOneSide) {
            translate([xSize-dbDepth, dbDepth, 0]) 
                cylinder(r=toolRadius, h=thickness+MAGIC_OFFSET);
            translate([dbDepth, dbDepth, 0]) 
                cylinder(r=toolRadius, h=thickness+MAGIC_OFFSET);
        }
    }
}

module hole(x, y, r, thickness) {
    translate([x, y, -MAGIC_OFFSET_2]) {
        cylinder(h=thickness+MAGIC_OFFSET, r=r);
    }
}