include <drawer.scad>
include <drawerrack.scad>

/**
 * The functionality of this file will project and render any of the plates in the 
 * project, so they can be exported as dxf (e.g.). Note that the configuration 
 * of the intire rack will be read from configuration.scad.
 * TODO: I guess it is actually possible to override any settings from configuration.scad, by defining the setting on the command line.
 * 
 * Usage example 1 - export a part from the drawer rack
 * $ openscad export.scad --render -D"rack='back'"" -o back.dxf
 * param rack: defines the part of the rack to render. 
 * Valid values: back, side, top, bottom, hseparator, vseparator
 * NOTE: 'top' and 'bottom' will render the same output, just as the 'side' part 
 * can be used for both left and right side.
 * 
 * Usage example 2 - export a part for a drawer 
 * $ openscad export.scad --render -D"drawer='side'" -DposLeft=2 -DposTop=0 -o sied2_0.dxf
 * param drawer: defines the part of the drawer to reander.
 * Valid values: bottom, side, back, front
 * param posLeft: defines the position of the drawer from the left, like "3rd from the left"
 * Valid values: a number from 0 to the number of drawers in the width minus one. E.g. if four 
 * drawers has been defines, the maximum value for posLeft is 3. You can also view this number 
 * as "There are 2 drawers left of the one I want rendered."
 * NOTE: The entire rack can actually be assembled mirrord left/right flippen up/down or both/rotate.
 * param posTop: Defines the position of the drawer from the top. Same rules goes as for the posLeft.
 *
 * Examples on how to use the export modules directoly from scad 
 * Make a drawer part:
 * drawer="side"; posLeft=0; posTop=0;
 * Make a rack part:
 * rack="back";
 * NOTE: I recently discovered that as scad can not make a circle, mills and laser cutters might have problems cutting the 
  * exported parts. But if you open the scad file in freecad, you will actually get "real circles".
 */


export();
module export() {
    assert(!(isDefined(rack) && isDefined(drawer)), "You can only export one part at the time");
    assert(isDefined(rack) || isDefined(drawer), "You have to define one part to export");

    if(isDefined(rack)) {
    	if(rack == "back") {
    		Make2DBackPlate();
    	}
    	else if(rack == "side") {
    		Make2DVerticalPlate();
    	}
    	else if(rack == "top" || rack == "bottom") {
    		Make2DHorisontalPlate();
    	}
    	else if(rack == "hseparator") {
    		Make2DHorisontalSeparator();
    	}
    	else if(rack == "vseparator") {
    		Make2DVerticalSeparator();
    	}
    	else {
			echo("Value of rack:", rack);
    		assert(false, "Unknow value given");
    	}
    }
	else if(isDefined(drawer)) {
		assert(posLeft>=0 && posLeft<=(len(drawerWidths)-1), "Left position is out of range");
		assert(posTop>=0 && posTop<=(len(drawerHeights)-1), "Top position is out of range");

		if(drawer == "bottom") {
			Make2DDrawerBottom(posLeft, posTop);
		}
		else if(drawer == "side") {
			Make2DDrawerSide(posLeft, posTop);
		}
		else if(drawer == "back") {
			Make2DDrawerBack(posLeft, posTop);
		}
		else if(drawer == "front") {
			Make2DDrawerFront(posLeft, posTop);
		}
		else {
			echo("Value of drawer:", drawer);
			assert(false, "Unknow value given");
		}
	}    
}

/** 
 * Helper functions
 */
function isDefined(var) = var != undef;

