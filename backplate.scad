include <fingers.scad>

/**
 * The sizes (x and y) is the inner measurements excluding the depth of the fingers (which is 
 *
 * 
 * fingerWidth
 * fingerDepth: Default equals the thickness of the side boards
 * mortiseWidth: Default equals the separator borad thickness
 * mortiseLength: Default equals the finger width
 */
module backPlate(xSize, ySize, thickness) {
    xSize = xSize+2*fingerExtraLength;
    ySize = ySize+2*fingerExtraLength;

    translate([-fingerExtraLength, -fingerExtraLength, 0])
        cube([xSize, ySize, thickness]);
}

module backPlateFingers(xSize, ySize, thickness, fingerWidth, fingerDepth) {
    if(fingerDepth > 0) {
        fx = fingerSizeOdd(xSize, fingerWidth);
        fy = fingerSizeOdd(ySize, fingerWidth);
        echo("Back plate finger size (width, height):", fx, fy);
        
        fingerDepth = fingerDepth+fingerExtraLength;


        cof = fingerExtraLength+MAGIC_OFFSET_2; // Cut offset
        cf = fingerExtraLength+MAGIC_OFFSET; // Cut size
        cv = [fingerExtraLength+MAGIC_OFFSET, fingerDepth+fingerExtraLength, thickness+MAGIC_OFFSET];

        translate([-fingerExtraLength, 0, 0])
            plateFingers(xSize, ySize, thickness, fx, fy, fingerDepth, 0, fy, true, false, false, false);
        translate([fingerExtraLength, 0, 0])
            plateFingers(xSize, ySize, thickness, fx, fy, fingerDepth, 0, fy, false, true, false, false);
        translate([0, fingerExtraLength, 0])
            plateFingers(xSize, ySize, thickness, fx, fy, fingerDepth, 0, fy, false, false, true, false);
        translate([0, -fingerExtraLength, 0])
            plateFingers(xSize, ySize, thickness, fx, fy, fingerDepth, 0, fy, false, false, false, true);

        // Remove artifacts that the fingers leave behind
        translate([-cof, -cof, -MAGIC_OFFSET_2])
            cube(cv);
        translate([-cof, ySize-fingerDepth+cof, -MAGIC_OFFSET_2])
            cube(cv);
        translate([xSize, ySize-fingerDepth+cof, -MAGIC_OFFSET_2])
            cube(cv);
        translate([xSize, -cof, -MAGIC_OFFSET_2])
            cube(cv);
    }
}

module backPlateMortises(widths, heights, depth, offset, thickness, margin) {
    union() {
        // Mortises for vertical separaters
        for(i = [0:len(widths)-2]) {
            x = offset + 2*margin
                    + sum(widths, i) 
                    + i*(thickness+2*margin) 
                    + widths[i];
            for(j = [0:len(heights)-1]) {
                y = offset + margin
                        + sum(heights, j) 
                        + j*(thickness+2*margin) 
                        + heights[j]/4;
                translate([x, y, 0]) {
                    finger(thickness, heights[j]/2, depth, db["xy"], false);
                }
            }
        }
        // Mortises for horisontal separaters
        for(i = [0:len(heights)-2]) {
            y = offset + 2*margin 
                    + sum(heights, i) 
                    + i*(thickness+2*margin) 
                    + heights[i];
            for(j = [0:len(widths)-1]) {
                x = offset + margin
                        + sum(widths, j) 
                        + j*(thickness+2*margin) 
                        + widths[j]/4;
                translate([x, y, 0]) {
                    finger(widths[j]/2, thickness, depth, db["xy"], false);
                }
            }
        }
    }
}
