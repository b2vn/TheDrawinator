include <configuration.scad>
include <general.scad>
include <backplate.scad>
include <verticalplate.scad>
include <horisontalplate.scad>
include <horisontalseparator.scad>
include <verticalseparator.scad>



function drawerX(posFromRight, posFromTop) = 
    sidePlateThickness +
    drawerToRoomMargin + 
    sum(drawerWidths, posFromRight) + 
    posFromRight*(2*drawerToRoomMargin+drawerPlateThickness);
function drawerY(posFromRight, posFromTop) = 
    sidePlateThickness +
    drawerToRoomMargin + 
    sum(drawerHeights, posFromTop) + 
    posFromTop*(2*drawerToRoomMargin+drawerPlateThickness) + 
    drawerHeights[posFromTop];
function drawerZ(posFromRight, posFromTop) = backPlateThickness;
function drawerPos(posFromRight, posFromTop) =
    [ drawerX(posFromRight, posFromTop),
            drawerY(posFromRight, posFromTop),
            drawerZ(posFromRight, posFromTop) ];




module PlaceDrawerFrontPlate(posFromRight, posFromTop) {
    translate(drawerPos(posFromRight, posFromTop)) rotate([90, 0, 0]) {
        color([0.3, 0.5, 1]) translate([0, drawerDepth, 0]) {
            rotate([90, 0, 0]) {
                MakeDrawerFrontPlate(posFromRight, posFromTop);
            }
        }
    }
}

module MakeDrawerFrontPlate(posFromRight, posFromTop) {
    ySize = drawerHeights[posFromTop];
    xSize = drawerWidths[posFromRight];


    difference() {
        horisontalPlate(xSize, ySize, drawerPlateThickness);
        horisontalPlateFingers(xSize, ySize, drawerPlateThickness, drawreFingerLength, drawerPlateThickness, drawerPlateThickness);

        if(drawerHandle == HandleType("none")) {
            // Do nothing... this is only here to avoid the assert at the end to flip out.
        }
        else if(drawerHandle == HandleType("hole")) {
            hole(xSize/2, ySize-drawerHandleHoleCenterFromTop, drawerHandleHoleDiameter/2, drawerPlateThickness);
        }
        else if(drawerHandle == HandleType("curve")) {
            // TODO: Is this really necessary? The hole setting with a negative distance to the top is quite nice and simple
        }
        else if(drawerHandle == HandleType("mark")) {
            r = drawerHandleMarkDiameter/2;
            y = ySize-drawerhandlerMarkCenterFromTop;
            hole(xSize/2-drawerHandlerMarkDistance/2, y, r, drawerPlateThickness);
            if(drawerHandlerMarkDistance>0)
                hole(xSize/2+drawerHandlerMarkDistance/2, y, r, drawerPlateThickness);
        }
        else if(drawerHandle == HandleType("cut")) {
            // TODO: The sides will also have to be changed for this to work...
        }
        else {
            // TODO: Assert!!!
        }
    }
}

module Make2DDrawerFront(posFromRight, posFromTop) {
    projection() MakeDrawerFrontPlate(posFromRight, posFromTop);
}

module PlaceDrawerBackPlate(posFromRight, posFromTop) {
    translate(drawerPos(posFromRight, posFromTop)) rotate([90, 0, 0]) {
        color([0.3, 0, 1]) translate([0, drawerPlateThickness, 0]) rotate([90, 0, 0]) 
            MakeDrawerBackPlate(posFromRight, posFromTop);
    }
}

module MakeDrawerBackPlate(posFromRight, posFromTop) {
    ySize = drawerHeights[posFromTop];
    xSize = drawerWidths[posFromRight];

    difference() {
        horisontalPlate(xSize, ySize, drawerPlateThickness);
        horisontalPlateFingers(xSize, ySize, drawerPlateThickness, drawreFingerLength, drawerPlateThickness, drawerPlateThickness);
    }
}

module Make2DDrawerBack(posFromRight, posFromTop) {
    projection() MakeDrawerBackPlate(posFromRight, posFromTop);
}

module PlaceDrawerSidePlate(posFromRight, posFromTop) {
    translate(drawerPos(posFromRight, posFromTop)) rotate([90, 0, 0]) {
        color([1, 0, 0]) {
            translate([drawerPlateThickness, 0, 0]) rotate([0, -90, 0]) {
                MakeDrawerSidePlate(posFromRight, posFromTop);
            }
            translate([drawerWidths[posFromRight], 0, 0]) rotate([0, -90, 0]) {
                MakeDrawerSidePlate(posFromRight, posFromTop);
            }
        }
    }
}
module MakeDrawerSidePlate(posFromRight, posFromTop) {
    xSize = drawerHeights[posFromTop];
    ySize = drawerDepth; 

    difference() {
        verticalPlate(xSize, ySize, drawerPlateThickness);
        verticalPlateFingers(xSize, ySize, drawerPlateThickness, drawreFingerLength, drawerPlateThickness, drawerPlateThickness);
    }
}

module Make2DDrawerSide(posFromRight, posFromTop) {
    projection() MakeDrawerSidePlate(posFromRight, posFromTop);
}

module PlaceDrawerBottomPlate(posFromRight, posFromTop) {
    translate(drawerPos(posFromRight, posFromTop)) rotate([90, 0, 0]) {
        MakeDrawerBottomPlate(posFromRight, posFromTop);
    }
}

module MakeDrawerBottomPlate(posFromRight, posFromTop) {
    xSize = drawerWidths[posFromRight];
    ySize = drawerDepth; 

    difference() {
        backPlate(xSize, ySize, drawerPlateThickness);
        backPlateFingers(xSize, ySize, drawerPlateThickness, drawreFingerLength, drawerPlateThickness);
    }
}

module Make2DDrawerBottom(posFromRight, posFromTop) {
    projection() MakeDrawerBottomPlate(posFromRight, posFromTop);
}
