include <fingers.scad>

module verticalPlate(xSize, ySize, thickness) {
    xSize = xSize+fingerExtraLength;
    ySize = ySize+2*fingerExtraLength;


    // // TODO: Move the finger actifact resulotion to the finger module below...
    // difference() {
        translate([-fingerExtraLength, -fingerExtraLength, 0])
            cube([xSize, ySize, thickness]);
    //     // Remove artifact in the buttom corners of this plate, that is not cut away when the fingers are made.
    //     translate([-2*fingerExtraLength, -2*fingerExtraLength, -MAGIC_OFFSET_2])
    //         cube([3*fingerExtraLength, 3*fingerExtraLength, thickness+MAGIC_OFFSET]);
    //     translate([-2*fingerExtraLength, ySize-2*fingerExtraLength, -MAGIC_OFFSET_2])
    //         cube([3*fingerExtraLength, 3*fingerExtraLength, thickness+MAGIC_OFFSET]);
    // }   
}

module verticalPlateFingers(xSize, ySize, thickness, fingerWidth, fingerDepth1, fingerDepth2) {
    fx = fingerSize(xSize, fingerWidth);
    fy = fingerSizeOdd(ySize, fingerWidth);
    echo("Vertical/side finger size (depth/height, height):", fx, fy);

    fingerDepth1=fingerDepth1+fingerExtraLength;
    fingerDepth2=fingerDepth2+fingerExtraLength;

    translate([0, -fingerExtraLength, 0])
        plateFingers(xSize, ySize, thickness, fx, fy, fingerDepth1, 0, 0, false, false, false, true);
    translate([0, fingerExtraLength, 0])
        plateFingers(xSize, ySize, thickness, fx, fy, fingerDepth1, 0, 0, false, false, true, false);
    translate([-fingerExtraLength, 0, 0])
        plateFingers(xSize, ySize, thickness, fx, fy, fingerDepth2, 0, 0, true, false, false, false);
    // Remove artifact in the buttom corners of this plate, that is not cut away when the fingers are made.
    translate([-2*fingerExtraLength, -2*fingerExtraLength, -MAGIC_OFFSET_2])
        cube([3*fingerExtraLength, 3*fingerExtraLength, thickness+MAGIC_OFFSET]);
    translate([-2*fingerExtraLength, ySize-fingerExtraLength, -MAGIC_OFFSET_2])
        cube([3*fingerExtraLength, 3*fingerExtraLength, thickness+MAGIC_OFFSET]);
}

module verticalPlateMortises(width, heights, depth, offset, thickness, margin) {
    for(i = [0:len(heights)-2]) {
        y = depth + 2*margin 
                + sum(heights, i) 
                + i*(thickness+2*margin) 
                + heights[i];
        translate([offset+width/4, y, 0]) 
            finger(width/2, thickness, depth, db["xy"], false);
        // TODO: Should the separators have fingers on the sides instead?
    }
}