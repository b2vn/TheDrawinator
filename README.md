This openscad project will generate cad files for all part needed to make a drawer rack and the drawers. All part are joined with finger joints, and can be milled or lasercut.

All you need to do, is specify the sizes of the individual drawers and the thickness of the materials. Optionally lots of other configuration can be made, to adjust the e.g. the drawer front, handles, labels, and much more (see configuration.scad for the full list of options).

See some of the features on youtube:

[![Demonstration video](https://img.youtube.com/vi/OOyxjzzIM8U/0.jpg)](https://youtu.be/OOyxjzzIM8U)

File overview:
* configuration.scad: This is where you comfigure the drawers and rack as you want you system to look
* maker.scad: This is mainly used for preview at the configuration. You can also export the parts from here.
* export.scad: This script is used by to bash script to generate all part automatically. However, you can also used this file directly tp get the parts you want to export. See the comments in the file for more information.
