include <types.scad>

// Plate thickness'
backPlateThickness = 9.5;
sidePlateThickness = 9.5;
separatorThickness = 9.5;
drawerPlateThickness = 11; 

// Outer dimensions of the drawers
drawerWidths = [100, 100, 100]; // Add as many numbers as you was the rack to be wide
drawerHeights = [55, 80, 80]; // Add as many numbers as you want the rack to be high
drawerDepth = 150; // TODO: Unused // Depth of the drawers and separaters. Is the same for all drawers
drawerRoomDepth = 170; // Is the same for all rooms
drawerToRoomMargin = 1; // The tollerence on the side of each drawer when it is in its room

// Drawer frontside 
drawerHandle = HandleType("hole"); 
// - Only applicable for hole handle
drawerHandleHoleDiameter = 50; 
drawerHandleHoleCenterFromTop = -5; // Distance from the top of the drawer to the center of the hole. Note: This number can also be 0 for half a circle or negative for just an arch
// - Only applicable for curve handle
drawerHandleCurveDepth = 10; // TODO: unused
drawerHandleCurveWidth = 30; // TODO: unused
// - Only applicable for mark handles
drawerHandleMarkDiameter = 1; 
drawerHandlerMarkDistance = 40; // Distance between the marks, or 0 for a single mark.
drawerHandleMarkDepth = 2; //TODO: unused 
drawerhandlerMarkCenterFromTop = 15;
// - Only applicable for cut handle
drawerHandleCutDepth = 25; //TODO: unused // How much lower the front should be that the other sides
drawerHandleCutAngledSides = true; // TODO: unused // Slope the side at a 45 degree angle down to the front.

// The label on the drawers
drawerLabelBevelIndentation = 1; // TODO : Unused // How deep the bevel around the label shoud be. 0 for flush with the front.
drawerLabelTextProtrusion = 1; // TODO: unused
drawerLabelWidth = 50; // TODO : Unused
drawerLabelHeight = 30; // TODO : Unused
drawerLabel1 = "6x80/35"; // TODO : Unused
drawerLabel2 = "Pan head T30"; // TODO : Unused
drawerLabelFontSize1 = 16; // TODO : Unused
drawerLabelFontSize2 = 10; // TODO : Unused

// Joinery
// NOTE: The finger width might not be exactly the size you specify here. If the overall size of the rack is not a multiple if the finger width, the width will be adjusted to the nearest possible size. 
rackFingerLength = 30; // Note: Finger length must be longer than the material thickness, or some corners will be cut off. TODO: Make an assertion on this.
drawreFingerLength = 30;
fingerPlayRoom = 0.8; // If you set this to 0, the finger and the gabs between will be the exact same size, and you will most likely not be able to join the part you make. Use this setting to a bit of wiggleroom and make room for some glue. 
fingerExtraLength = 0; // TODO: Unused // If this is 0, the finger depth will be exactly the thickness of the plate it is joining. Incresing this ensures that you get some overlap, that you will then have to remove with a planer, sandpaper, or similar.
separatorSlotExtraLength = 0; // TODO: Unused // Same as above, but for the tenons on the separators. You can enter a negative number if you do not want the separators to go all the way through the sides/back.

// Machine 
toolDiameter = 0; // This is used to create "dogbones" in all the connection. If you don't want the drawings with dogbones, but rather want your CAM to handle that, use 0.

