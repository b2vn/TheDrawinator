include <drawer.scad>
include <drawerrack.scad>
 
// For the drawers
for(drawerPositionFromRight = [0:len(drawerWidths)-1]) {
    for(drawerPositionFromTop = [0:len(drawerHeights)-1]) {
    drawerPositionFromRight=0;
    drawerPositionFromTop=0;

        PlaceDrawerBottomPlate(drawerPositionFromRight, drawerPositionFromTop);
        PlaceDrawerSidePlate(drawerPositionFromRight, drawerPositionFromTop);
        PlaceDrawerBackPlate(drawerPositionFromRight, drawerPositionFromTop);
        PlaceDrawerFrontPlate(drawerPositionFromRight, drawerPositionFromTop);
    }
}
    
// For the rack
PlaceBackPlate();
if(sidePlateThickness > 0) {
	PlaceVerticalPlate();
	PlaceHorisontalPlate();
}
PlaceHorisontalSeparator();
PlaceVerticalSeparator();


//Make2DDrawerBack(0, 0);