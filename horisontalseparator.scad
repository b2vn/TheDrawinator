include <fingers.scad>

module horisontalSeparator(xSize, ySize, thickness) {
    xSize = xSize+2*separatorSlotExtraLength;
    ySize = ySize+separatorSlotExtraLength;

    translate([-separatorSlotExtraLength, -separatorSlotExtraLength, 0])
        cube([xSize, ySize, thickness]);
}

module horisontalSeparatorFingers(xSize, ySize, widths, depth, thickness, fingerDepthX, fingerDepthY, margin) {
    if(fingerDepthY > 0) {
        fingerDepthY = fingerDepthY+separatorSlotExtraLength;
    }
    if(fingerDepthX > 0) {
        fingerDepthX = fingerDepthX+separatorSlotExtraLength;
    }

    if(fingerDepthY > 0) {
        translate([0, -separatorSlotExtraLength, 0]) {
            // cut first along the x axis
            finger(fingerDepthY+widths[0]/4+margin/2, fingerDepthX, thickness);
            
            for(j = [0:len(widths)-2]) {
                x = fingerDepthY + margin // Offset the thickness of the side wall + one margen
                        + sum(widths, j) // Offset the width of all the drawers so far
                        + j*(thickness+2*margin) // Add 1 thickness og the vertical separator and 2 margins for each drawers so far
                        + 3*widths[j]/4; // and since the mortise is half the width of the drawer, 3/4 of the drawer width will bring to the end of the slot
                w = widths[j]/4 + 2*margin + thickness + widths[j+1]/4;
                translate([x, 0, 0]) {
                    finger(w, fingerDepthX, thickness);
                }
            }

            // Make the last cut on the x axis
            j = len(widths)-1;
            x = fingerDepthY + margin // Offset the thickness of the side wall + one margen
                    + sum(widths, j) // Offset the width of all the drawers so far
                    + j*(thickness+2*margin) // Add 1 thickness og the vertical separator and 2 margins for each drawers so far
                    + 3*widths[j]/4; // and since the mortise is half the width of the drawer, 3/4 of the drawer width will bring to the end of the slot
            w = widths[j]/4 + margin + fingerDepthY;
            translate([x, 0, 0]) {
                finger(w, fingerDepthX, thickness);
            }

            // TODO: Remove the artifacts in the corners that were not removed by the fingers
        }

        // Cut the y axis
        h = fingerDepthX+depth/4; // Start of the slot

        translate([xSize+separatorSlotExtraLength, 0, 0]) {
            translate([0, 0, 0])
                rotate([0, 0, 90])
                    finger(h, fingerDepthY, thickness);
            translate([0, h+depth/2, 0])
                rotate([0, 0, 90])
                    finger(depth/4, fingerDepthY, thickness);
        }

        translate([-separatorSlotExtraLength, 0, 0]) {
            translate([0, h, 0])
                rotate([0, 0, -90])
                    finger(h+fingerDepthX+separatorSlotExtraLength, fingerDepthY, thickness);
            translate([0, depth/4+h+depth/2, 0]) // depth/4: to get the finger lifted to the xy plane. h: to get the lifted pass the back plate. depth/2 to get it pass the slot
                rotate([0, 0, -90])
                    finger(depth/4, fingerDepthY, thickness);
        }
    }
}

module horisontalSeparatorSlots(widths, thickness, depth, offsetX, offsetY, margin) {
    // The horisontal separaters slots must be cut from the front, due to the extra strength needed on the vertical sepataters.
    for(i = [1:len(widths)-1]) {
        x = offsetX
                + sum(widths, i) 
                + i*(thickness+2*margin);
        translate([x, offsetY+depth, 0]) 
            rotate([0, 0, 180])
                finger(thickness, depth/2, thickness);
    }
}