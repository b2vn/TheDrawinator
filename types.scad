// Typedefs
function HandleType(type="hole") =
	_HandleType[search([type], _HandleType, 1, 0)[0] ][1];
_HandleType= [ 
        ["none",0],     // Plain straight front
        ["hole",1],     // One finger size hole in the front
        ["curve",2],    // A curve on the top of the drawer
        ["mark", 3],    // Marking for drillig, to attach a handle
        ["cut", 4],     // Make the fron side lower that the other sides
];

// Magic constants to help scad to get rid of infinit thin walls
MAGIC_OFFSET=0.1;
MAGIC_OFFSET_2=MAGIC_OFFSET/2;
MAGIC_OFFSET_4=MAGIC_OFFSET/4;
