toolRadius = toolDiameter/2; // The radius is used mush more the the diameter in the calculations

/**
 * Return the sum of the first n (or all if n is not defined) elements of the vector v.
 * Note: I and S are the tail recursion variables and should not be given as arguments
 */
function sum(v, n=-1, I=0, S=0) =
    I==len(v) || n==0 ? S : sum(v, n-1, I+1, S+v[I]);

function width() = 
    sum(drawerWidths) 
            + 2*drawerToRoomMargin*len(drawerWidths) 
            + separatorThickness*(len(drawerWidths)-1)
            + 2*sidePlateThickness;

function height() = 
    sum(drawerHeights) 
            + 2*drawerToRoomMargin*len(drawerHeights) 
            + separatorThickness*(len(drawerHeights)-1)
            + 2*sidePlateThickness;

function isOdd(n) = 
    n%2==1;

function isMultiple(n, d) =
    n%d == 0;

function isOddMultiple(n, d) =
    isMultiple(n, d) && isOdd(n/d);
    
// A function to force the rendering to quite by introducing an infint loop. The compiler detects this and assert the compilation. Use like echo("",  abort());
function abort() = 
    abort();

// Functions to calculate the adaptive finger width. This ensures that you can make any sice drawers and box, without having to have them being a multiple of the finger size
function numberOfFingers(d, f) =
    round(d/f);
    
function fingerSize(d, f, extra=0) = 
    (d/(numberOfFingers(d, f)+extra));

function fingerSizeOdd (d, f) = 
    numberOfFingers(d, f)%2 
            ? fingerSize(d, f)  // Allready an odd number of fingers
            : ( numberOfFingers(d, f) > (d/f) // Even number of fingers, get it odd...
                    ? fingerSize(d, f, 1)
                    : fingerSize(d, f, -1) );
